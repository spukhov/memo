package com.eightbitcenter.utils;

import com.eightbitcenter.service.NoteService;
import com.eightbitcenter.service.UserService;
import org.joda.time.DateTime;
import org.joda.time.DateTimeFieldType;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Date;

public class MemoDay {
    @Autowired
    NoteService noteService;
    @Autowired
    UserService userService;

    public MemoDay (DateTime day) {
       this.day = day;
    }

    private DateTime day;
    private String color = "#000";
    private int number;

    public DateTime getDay() {
        return day;
    }

    public void setDay(DateTime day) {
        this.day = day;
    }

    public String getColor() {
        if(day.get(DateTimeFieldType.monthOfYear())!=new DateTime().get(DateTimeFieldType.monthOfYear()))
            return "#ccc";
        else if (day.get(DateTimeFieldType.dayOfYear())==(new DateTime().get(DateTimeFieldType.dayOfYear()))) {
            return "orange";
        }   else
            return "#000";
    }

    public void setColor(String color) {
        this.color = color;
    }

    public Date getUtilDate(){
        return day.toDate();
    }

    public String getFormattedDate() {
        DateTimeFormatter fmt = DateTimeFormat.forPattern("MMMM, yyyy");
        return fmt.print(day);
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }
}

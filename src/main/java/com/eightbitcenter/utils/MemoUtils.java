package com.eightbitcenter.utils;

import org.apache.log4j.Logger;
import org.joda.time.DateTime;
import org.joda.time.DateTimeFieldType;

import java.text.DateFormatSymbols;
import java.util.*;

public class MemoUtils {
    public static Logger LOG = Logger.getLogger(MemoUtils.class);

    public static enum Currency{
        UHR("uhr"),USD("usd"),EUR("eur");
        private String name;
        Currency(String name){
            this.name=name;
        }
    }

    /**
     *
     *
     * @return
     */

    public static List<MemoWeek> getDaysOfCurrentMonth(Map<DateTime, Integer> noteMap) {
        List<MemoWeek> weeks = new ArrayList<MemoWeek>();
        DateTime currentDay = new DateTime();
        MemoDay dayForEditing = new MemoDay(new DateTime());

        DateTime firstDayOfMonth = new DateTime(currentDay);
        firstDayOfMonth = firstDayOfMonth.withDayOfMonth(1);
        DateTime testDay = new DateTime(firstDayOfMonth);
        int firstDayOfMonthWeekDayNumber = firstDayOfMonth.get(DateTimeFieldType.dayOfWeek());
        int daysInCurrentMonth = currentDay.plusMonths(1).withDayOfMonth(1).minusDays(1).get(DateTimeFieldType.dayOfMonth());
        //System.out.println(daysInCurrentMonth);

        // 1 - get days before current month
        testDay = testDay.minusDays(firstDayOfMonthWeekDayNumber - 1);
        MemoWeek week = new MemoWeek();
        for (int i = 1; i <=7; i++) {
            dayForEditing =  new MemoDay(testDay);
            dayForEditing.setNumber(getNumberForDate(dayForEditing.getDay()
                    .withTimeAtStartOfDay(), noteMap));
            week.setDayByNumber(i, dayForEditing);
            //System.out.println(i);
            testDay = testDay.plusDays(1);
        }
        weeks.add(week);
        // 2 - get days of current month
        int i = 1;
        int j = 1;
        int max = 28 + getAdvancedDays();
        System.out.println("===== " + max);
        while (i <= max){
            week = new MemoWeek();
            while(j<=7){
                dayForEditing =  new MemoDay(testDay);
                dayForEditing.setNumber(getNumberForDate(dayForEditing.getDay()
                        .withTimeAtStartOfDay(), noteMap));
                week.setDayByNumber(j, dayForEditing);
                testDay = testDay.plusDays(1);
                //System.out.println(i);
                i++;
                j++;
                if(i > max) break;
            }
            weeks.add(week);
            j=1;
        }

        //GregorianCalendar day = new GregorianCalendar(2013, 11, 5);


        return weeks;
    }

    private static int getNumberForDate(DateTime date, Map<DateTime, Integer> map){
        LOG.info(" ---utils--- getNumberForDate :" + date + "; map size:" + map.keySet().size());
        if (map.get(date)==null) return 0;
        return map.get(date);
    }


    public static String getCurrentMonthString(int i) {
        String month = "error";
        DateFormatSymbols dfs = new DateFormatSymbols();
        String[] months = dfs.getMonths();
        if (i >= 0 && i <= 11) {
            month = months[i];
        }
        return month;
    }

    public static int getAdvancedDays(){
        int days = 0;
        int firstWeek = new DateTime().withDayOfMonth(1).get(DateTimeFieldType.weekOfWeekyear());
        int lastWeek = new DateTime().plusMonths(1)
                .withDayOfMonth(1)
                .minusDays(1)
                .get(DateTimeFieldType.weekOfWeekyear());
        System.out.println("Weeks for calendar: " + (lastWeek-firstWeek));
        if(lastWeek-firstWeek>4) {
            return 7;
        }

        return days;
    }
}

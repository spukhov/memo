package com.eightbitcenter.utils;

import java.util.ArrayList;
import java.util.Arrays;

import org.joda.time.DateTimeFieldType;

import java.util.List;

public class MemoWeek {
    private List<MemoDay> days = new ArrayList<MemoDay>();
    private MemoDay monday;
    private MemoDay tuesday;
    private MemoDay wednesday;
    private MemoDay thursday;
    private MemoDay friday;
    private MemoDay saturday;
    private MemoDay sunday;

    public void setDayByNumber(int number, MemoDay day) {
        if (number == 1) setMonday(day);
        else if (number == 2) setTuesday(day);
        else if (number == 3) setWednesday(day);
        else if (number == 4) setThursday(day);
        else if (number == 5) setFriday(day);
        else if (number == 6) setSaturday(day);
        else if (number == 7) setSunday(day);
        else throw new RuntimeException("Incorrect day number!");
    }

    public int getDayOfMonthNumber(MemoDay day){
        return day.getDay().get(DateTimeFieldType.dayOfMonth());
    }
    public String getDayColorString(MemoDay day){
        return day.getColor();
    }

    public List<MemoDay> getDays() {
        //return days;
        return Arrays.asList(new MemoDay[] {monday,tuesday,wednesday,thursday,friday,saturday,sunday});
    }

    public void setDays(List<MemoDay> days) {
        this.days = days;
    }

    public MemoDay getMonday() {
        return monday;
    }

    public void setMonday(MemoDay monday) {
        this.monday = monday;
    }

    public MemoDay getTuesday() {
        return tuesday;
    }

    public void setTuesday(MemoDay tuesday) {
        this.tuesday = tuesday;
    }

    public MemoDay getWednesday() {
        return wednesday;
    }

    public void setWednesday(MemoDay wednesday) {
        this.wednesday = wednesday;
    }

    public MemoDay getThursday() {
        return thursday;
    }

    public void setThursday(MemoDay thursday) {
        this.thursday = thursday;
    }

    public MemoDay getFriday() {
        return friday;
    }

    public void setFriday(MemoDay friday) {
        this.friday = friday;
    }

    public MemoDay getSaturday() {
        return saturday;
    }

    public void setSaturday(MemoDay saturday) {
        this.saturday = saturday;
    }

    public MemoDay getSunday() {
        return sunday;
    }

    public void setSunday(MemoDay sunday) {
        this.sunday = sunday;
    }
}

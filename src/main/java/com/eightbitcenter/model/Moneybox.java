package com.eightbitcenter.model;

import com.eightbitcenter.utils.MemoUtils;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;


@Document(collection = "moneyboxes")
public class Moneybox implements ModelMarker {
    @Id
    private String id;
    private String userId;
    private double finalAmount;
    private double actualAmount;
    private MemoUtils.Currency currency;

    public Moneybox(String userId, double finalAmount, double actualAmount, MemoUtils.Currency currency) {
        this.userId = userId;
        this.finalAmount = finalAmount;
        this.actualAmount = actualAmount;
        this.currency = currency;
    }

    public Moneybox(){

    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public double getFinalAmount() {
        return finalAmount;
    }

    public void setFinalAmount(double finalAmount) {
        this.finalAmount = finalAmount;
    }

    public double getActualAmount() {
        return actualAmount;
    }

    public void setActualAmount(double actualAmount) {
        this.actualAmount = actualAmount;
    }

    public MemoUtils.Currency getCurrency() {
        return currency;
    }

    public void setCurrency(MemoUtils.Currency currency) {
        this.currency = currency;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }
}

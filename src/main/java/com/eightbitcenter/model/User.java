package com.eightbitcenter.model;

import java.util.Arrays;
import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import com.eightbitcenter.security.Role;

@Document(collection = "users")
public class User {
	@Id
	private String id;

	private String email;

	private String password;

	private String firstname;

	private String secondname;

	private List<Role> roles = Arrays.asList(new Role("ROLE_USER"));

	public List<Role> getRoles() {
		return roles;
	}

	public void setRoles(List roles) {
		this.roles = roles;
	}

	public User(String email, String password) {
		this.email = email;
		this.password = password;
	}

	public User(String email, String password, String firstname,
			String secondname) {
		this.email = email;
		this.password = password;
		this.firstname = firstname;
		this.secondname = secondname;
	}

	public User() {

	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
}

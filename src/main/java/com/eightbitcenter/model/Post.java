package com.eightbitcenter.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.io.Serializable;

@Document(collection = "posts")
public class Post implements ModelMarker,Serializable {
    @Id
    private String id;

    private String userId;

    private String topic;
    private String color;
    private String message;

    public Post(){

    }

    public Post(String userId, String topic, String color, String message){
        this.message = message;
        this.userId = userId;
        this.topic = topic;
        this.color = color;
    }

    public String getUserId() {
        return userId;
    }
    
    public String getPostId() {
        return id;
    }
    
    public String setPostId(String postId) {
        return id=postId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getTopic() {
        return topic;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }
    
    public void setColor(String color)
    {
    	this.color = color;	
    }

    @Override
    public String getId() {
        return id;
    }
}

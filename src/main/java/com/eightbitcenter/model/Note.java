package com.eightbitcenter.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.io.Serializable;
import java.util.Date;

@Document(collection = "notes")
public class Note implements Comparable<Note>, Serializable, ModelMarker{
    @Id
    private String id;
    private String userId;
    private String time;
    private String title;
    private String text;
    private Date date;


    public Note(String userId, String time, String title, String text, Date date) {
        this.userId = userId;
        this.time = time;
        this.title = title;
        this.text = text;
        this.date = date;
    }

    public Note(){

    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getText() {
        return text;
    }

    public String getShortText() {
        if (text.length()>75) return text.substring(0,75).concat("...");
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    @Override
    public int compareTo(Note o) {
        Double thisTime = Double.parseDouble(this.getTime().replace(':','.'));
        Double oTime = Double.parseDouble(o.getTime().replace(':', '.'));
        return (thisTime > oTime) ? 1 : (thisTime < oTime) ? -1 : 0;
    }
}

package com.eightbitcenter.rest;

import com.eightbitcenter.dao.NoteDAO;
import com.eightbitcenter.dao.UserDAO;
import com.eightbitcenter.model.Note;
import com.eightbitcenter.model.User;
import com.sun.jersey.api.core.InjectParam;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.xml.ws.Response;
import java.util.Date;
import java.util.List;

@Component
@Scope("request")
@Path("/user")
public class UserServiceREST {
    @InjectParam
    private UserDAO userDAO;

    @POST
    @Path("/validate")
    @Produces(MediaType.TEXT_PLAIN)
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    public String validateUser(@FormParam("email") String email, @FormParam("pass") String pass){
        User user = new User ();
        user.setEmail(email);
        user.setPassword(pass);
        boolean isValid = userDAO.validateLogin(user);
        if (isValid) {
            return userDAO.getId();
        } else {
            return "false";
        }

    }

}

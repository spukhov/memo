package com.eightbitcenter.rest;

import com.eightbitcenter.dao.NoteDAO;
import com.eightbitcenter.dao.UserDAO;
import com.eightbitcenter.model.Note;
import com.eightbitcenter.service.UserService;
import com.sun.jersey.api.core.InjectParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.xml.ws.Response;
import java.util.Collections;
import java.util.Date;
import java.util.List;

@Component
@Scope("request")
@Path("/notes/{user}")
public class NoteServiceREST {
    @InjectParam
    private NoteDAO noteDAO;

    @GET
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public List<Note> get(@PathParam("user") String userId){
        System.out.println(new Date());

        List<Note> list = noteDAO.find(userId, new Date());
        Collections.sort(list);
        System.out.println(list);
        return list;
    }

}

package com.eightbitcenter.service;

import com.eightbitcenter.dao.MoneyboxDAO;
import com.eightbitcenter.model.Moneybox;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MoneyboxService {
    public static Logger LOG = Logger.getLogger(MoneyboxService.class);
    @Autowired
    private MoneyboxDAO moneyboxDAO;
    @Autowired
    private UserService userService;

    public void save(Moneybox m){
        System.out.println("1");
        moneyboxDAO.save(m);
    }

    public void delete(Moneybox m){
        moneyboxDAO.del(m);
    }

    public void updateActualAmount(double n) {
        moneyboxDAO.updateActualAmount(n);
    }

    public List<Moneybox> find(String userId){
        return moneyboxDAO.find(userId);
    }
}

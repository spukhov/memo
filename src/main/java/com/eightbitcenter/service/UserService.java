package com.eightbitcenter.service;

import com.eightbitcenter.dao.UserDAO;
import com.eightbitcenter.model.User;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

@Service
@Scope("prototype")
public class UserService {
	public static Logger LOG = Logger.getLogger(UserService.class);

	@Autowired
	private UserDAO userDAO;

	public void register(User user) {
		LOG.info("register()");
		userDAO.register(user);
	}


	 public boolean validateLogin(User user){ LOG.info("validateLogin()");
	 return userDAO.validateLogin(user); }

	public String getId() {
		return userDAO.getId();
	}

	public User findUserByName(String email) {
		return userDAO.findUserByName(email);
	}
}

package com.eightbitcenter.service;

import com.eightbitcenter.dao.NoteDAO;
import com.eightbitcenter.model.Note;
import org.apache.log4j.Logger;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.Map;

@Service
public class NoteService {
	public static Logger LOG = Logger.getLogger(NoteService.class);

	@Autowired
	private NoteDAO noteDAO;

	public void save(Note note) {
		LOG.info("save()");
		noteDAO.persist(note);
	}

	public List<Note> find(String userId, Date date) {
		LOG.info("find()");
		return noteDAO.find(userId, date);
	}

	public void delete(Note note) {
		LOG.info("deleteNote()");
        noteDAO.del(note);
	}

	public void update(Note note) {
        noteDAO.update(note);
	}
    public boolean isDayEmpty(String userId, Date date){
        return noteDAO.isDayEmpty(userId,date);
    }

    public Map<DateTime, Integer> findNotesForMonth(String userId, Date date){
        return noteDAO.findNotesForMonth(userId,date);
    }
}

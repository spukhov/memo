package com.eightbitcenter.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.eightbitcenter.dao.UserDAO;
import com.eightbitcenter.model.User;
import com.eightbitcenter.security.Role;

@Service
public class CustomMongoSecurityService implements UserDetailsService {

	private User user;

	@Autowired
	private UserDAO userDAO;

	@Override
	public UserDetails loadUserByUsername(String email)
			throws UsernameNotFoundException {
		this.user = userDAO.findUserByName(email);
		return null;
	}

	public List getGrantedAuthorities() {

		List authorities = new ArrayList();
		for (Role role : user.getRoles()) {
			authorities.add(new SimpleGrantedAuthority(role.getRole()));
			System.out.println("RRRRRRRRRRRRRRRRRRRRRRR"+" "+role.getRole());
		}

		return authorities;
	}
}

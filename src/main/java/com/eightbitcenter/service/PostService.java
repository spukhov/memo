package com.eightbitcenter.service;

import com.eightbitcenter.dao.PostDAO;
import com.eightbitcenter.model.Post;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PostService {
	public static Logger LOG = Logger.getLogger(PostService.class);

	@Autowired
	private PostDAO postDAO;

	public void savePost(Post post) {
		LOG.info("savePost()");
		postDAO.savePost(post);
	}

	public List<Post> getPosts(String userId) {
		LOG.info("getPosts() - STEP 2");
		return postDAO.getPosts(userId);
	}

	public void deletePost(Post post) {
		LOG.info("deletePost()");
		postDAO.deletePost(post);
	}

	public void updatePost(Post post, String newTopic, String newMessage,
			String newColor) {
		postDAO.updatePost(post, newTopic, newMessage, newColor);
	}

}

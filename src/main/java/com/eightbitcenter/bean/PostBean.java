package com.eightbitcenter.bean;

import com.eightbitcenter.dao.UserDAO;
import com.eightbitcenter.model.Post;
import com.eightbitcenter.service.PostService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;

@Component
@ManagedBean
@RequestScoped
public class PostBean {
    public static Logger LOG = Logger.getLogger(PostBean.class);
    private String topic;
    private String color;
    private String message;

    @Autowired
    private UserDAO userDao;
    @Autowired
    private PostService postService;
    private Post editedPost;

    public void savePost() {
        LOG.info("savePost()");
        color = (color == null) ? "rgba(65, 65, 65, 0.25)" : color;
        postService.savePost(new Post(userDao.getId(), topic, color, message));
    }

    public void setEditedPost(Post post) {
        editedPost = post;
    }

    public void deletePost() {
        if (editedPost != null) {
            postService.deletePost(editedPost);
        }
    }

    public void updatePost() {
        if (editedPost != null) {
            postService.updatePost(editedPost, topic, message, color);
            topic = null;
            message = null;
            color = null;
        }
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getTopic() {
        return topic;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }
}

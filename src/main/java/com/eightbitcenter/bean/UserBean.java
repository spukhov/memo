package com.eightbitcenter.bean;

import com.eightbitcenter.model.Moneybox;
import com.eightbitcenter.model.Note;
import com.eightbitcenter.model.Post;
import com.eightbitcenter.model.User;
import com.eightbitcenter.service.MoneyboxService;
import com.eightbitcenter.service.NoteService;
import com.eightbitcenter.service.PostService;
import com.eightbitcenter.service.UserService;
import com.eightbitcenter.utils.MemoUtils;
import org.apache.log4j.Logger;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;
import java.text.SimpleDateFormat;
import java.util.*;

@Component
@ManagedBean
@SessionScoped
@Scope("session")
public class UserBean {
    public static Logger LOG = Logger.getLogger(UserBean.class);
    private Locale locale;

    private String id;
    private String email;
    private String password;
    private String firstname;
    private String secondname;

    private boolean isLoggedIn = false;

    @Autowired
    UserService userService;
    @Autowired
    PostService postService;
    @Autowired
    NoteService noteService;
    @Autowired
    MoneyboxService moneyboxService;

    private Date day = new Date();   // a date to pick notes for mentioned date, by default = current day

    private List<Post> posts;
    private List<Note> notes;

    //private List<Moneybox> moneyboxes;

    public UserBean() {
    }

    public void initValues(){
        id = userService.getId();
        LOG.info("initialized id: " + id);
        posts = postService.getPosts(getId());
        notes = noteService.find(getId(), day);
        //initMoneyboxes();
    }

    /*public void initMoneyboxes(){
        moneyboxes = moneyboxService.find(userService.getId());
    }*/

    public String register() {
        LOG.info("resister()");
        userService.register(new User(email, password, firstname,
                secondname));

        return validateLogin();
    }

    public String validateLogin() {
        LOG.info("validateLogin()");
        if (userService.validateLogin(new User(email, password))) {
            System.out.println("TRUUUE");
            HttpSession session = (HttpSession)
                    FacesContext.getCurrentInstance().getExternalContext().
                            getSession(false);
            session.setAttribute("username", getId());
            isLoggedIn = true;
            initValues();
            return "calendar";
        } else {
            System.out.println("FAAALSE");
            FacesContext.getCurrentInstance()
                    .addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Autorization failed", null));
            isLoggedIn = false;
            return null;
        }
    }

    public String logout() {
        HttpSession session = (HttpSession) FacesContext.getCurrentInstance().
                getExternalContext().
                getSession(false);
        session.invalidate();
        return "/login.xhtml";
    }

    public void findUserByName(String email) {
        User user = userService.findUserByName(email);
        System.out.println(user.getEmail());

    }

    public List<Post> getPosts() {
        return postService.getPosts(getId());
    }

    public void setPosts(List<Post> posts) {
        this.posts = posts;
    }

    public List<Note> getNotes() {
        notes = noteService.find(getId(), day);
        Collections.sort(notes);
        return notes;
    }

    public void setNotes(List<Note> notes) {
        this.notes = notes;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getSecondname() {
        return secondname;
    }

    public void setSecondname(String secondname) {
        this.secondname = secondname;
    }

    public String getId() {
        return id;
    }

    public Locale getLocale() {
        return locale;
    }

    public void setLocale(Locale locale) {
        this.locale = locale;
    }

    public Date getDay() {
        return day;
    }

    public void setDay(Date day) {
        this.day = day;
    }

    public String getDateFormatted() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("MMMM, dd");
        return dateFormat.format(day);
    }

    public boolean isLoggedIn() {
        return isLoggedIn;
    }

    public void setLoggedIn(boolean loggedIn) {
        isLoggedIn = loggedIn;
    }

}

package com.eightbitcenter.bean;

import com.eightbitcenter.dao.MoneyboxDAO;
import com.eightbitcenter.model.Moneybox;
import com.eightbitcenter.service.MoneyboxService;
import com.eightbitcenter.service.UserService;
import com.eightbitcenter.utils.MemoUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import java.util.List;

@Component
@ManagedBean(name = "moneyboxBean")
@SessionScoped
public class MoneyboxBean {

    private double finalAmount;
    private double actualAmount;
    private double moneyToAdd;
    private MemoUtils.Currency currency;
    private int percentage;
    private Moneybox moneybox;

    private MemoUtils.Currency[] currencies = MemoUtils.Currency.values();

    @Autowired
    private MoneyboxService moneyboxService;
    @Autowired
    private UserService userService;

    public void addMoneybox () {
        Moneybox box = new Moneybox( userService.getId(), finalAmount, actualAmount, currency);
        moneyboxService.save(box);
        init();
    }

    public void init(){
        getMoneyboxes();
        getFinalAmount();
        getActualAmount() ;
        getPercentage();
    }

    public void addMoney(){
        actualAmount+=moneyToAdd;
        moneyboxService.updateActualAmount(actualAmount);
        moneyToAdd=0;
        init();
    }

    public double getFinalAmount() {
        finalAmount = getMoneybox().getFinalAmount();
        return finalAmount;
    }

    public void setFinalAmount(double finalAmount) {
        this.finalAmount = finalAmount;
    }

    public double getActualAmount() {
        actualAmount = getMoneybox().getActualAmount();
        return actualAmount;
    }

    public void setActualAmount(double actualAmount) {
        this.actualAmount = actualAmount;
    }

    public MemoUtils.Currency getCurrency() {
        return currency;
    }

    public void setCurrency(MemoUtils.Currency currency) {
        this.currency = currency;
    }

    public double getMoneyToAdd() {
        return moneyToAdd;
    }

    public void setMoneyToAdd(double moneyToAdd) {
        this.moneyToAdd = moneyToAdd;
    }

    public int getPercentage() {
        return (int)Math.round(actualAmount * 100 / finalAmount);
    }

    public MemoUtils.Currency[] getCurrencies() {
        currency = getMoneybox().getCurrency();
        return currencies;
    }

    public List<Moneybox> getMoneyboxes(){
        List<Moneybox> list = moneyboxService.find(userService.getId());
        if (!list.isEmpty())
        moneybox = list.get(0);
        else moneybox = new Moneybox();
        return moneyboxService.find(userService.getId());
    }

    public Moneybox getMoneybox() {
        return moneybox;
    }
}

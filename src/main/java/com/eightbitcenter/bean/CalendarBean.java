package com.eightbitcenter.bean;

import com.eightbitcenter.service.NoteService;
import com.eightbitcenter.service.PostService;
import com.eightbitcenter.service.UserService;
import com.eightbitcenter.utils.MemoUtils;
import com.eightbitcenter.utils.MemoWeek;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

@Component
@ManagedBean(name = "calendarBean")
@SessionScoped
@Scope("session")
public class CalendarBean {
    public static Logger LOG = Logger.getLogger(CalendarBean.class);

    @Autowired
    UserService userService;
    @Autowired
    NoteService noteService;


    private List<MemoWeek> daysOfCurrentMonth;
    private String currentMonthString;

    @PostConstruct
    public void init(){
        daysOfCurrentMonth = MemoUtils.getDaysOfCurrentMonth(noteService
                .findNotesForMonth(userService.getId(), new Date()));
    }
    public void initValues(){
        daysOfCurrentMonth = MemoUtils.getDaysOfCurrentMonth(noteService
                .findNotesForMonth(userService.getId(), new Date()));
    }

    public String getCurrentMonthString() {
        return MemoUtils.getCurrentMonthString(Calendar.getInstance().get(
                Calendar.MONTH))
                + ", " + Calendar.getInstance().get(Calendar.YEAR);
    }

    public void setCurrentMonthString(String currentMonthString) {
        this.currentMonthString = currentMonthString;
    }

    public void setDaysOfCurrentMonth(List<MemoWeek> daysOfCurrentMonth) {
        this.daysOfCurrentMonth = daysOfCurrentMonth;
    }

    public List<MemoWeek> getDaysOfCurrentMonth() {
        return daysOfCurrentMonth;
    }
}

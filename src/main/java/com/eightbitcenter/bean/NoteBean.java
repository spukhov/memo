package com.eightbitcenter.bean;

import com.eightbitcenter.model.Note;
import com.eightbitcenter.service.NoteService;
import com.eightbitcenter.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.bean.ViewScoped;
import java.util.Date;

@Component
@ManagedBean(name = "noteBean")
@RequestScoped
public class NoteBean {
    private String hrs;
    private String mins;
    private String time = hrs + ":" + mins;
    private String userId;
    private String title;
    private String text;
    private Date date;
    private Note editedNote;

    @Autowired
    private UserService userService;
    @Autowired
    private NoteService noteService;

    public void save(){
        noteService.save(new Note(userService.getId(), time, title, text, date));
    }

    public String getTime() {
        System.out.println("!!!!! - " +  hrs + ":" + mins);
        return hrs + ":" + mins;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getHrs() {
        return hrs;
    }

    public void setHrs(String hrs) {
        this.hrs = hrs;
    }

    public String getMins() {
        return mins;
    }

    public void setMins(String mins) {
        this.mins = mins;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public Note getEditedNote() {
        return editedNote;
    }

    public void setEditedNote(Note editedNote) {
        this.editedNote = editedNote;
    }

    public void addNote(Date day) {
        Note note = new Note();
        note.setDate(day);
        note.setUserId(userService.getId());
        note.setText(text);
        note.setTime(getTime());
        note.setTitle(title);
        noteService.save(note);
    }

    public void deleteNote(){
        noteService.delete(editedNote);
    }

    public void updateNote(){
        noteService.update(editedNote);
    }
}

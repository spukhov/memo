package com.eightbitcenter.config;

import org.ocpsoft.rewrite.config.Configuration;
import org.ocpsoft.rewrite.config.ConfigurationBuilder;
import org.ocpsoft.rewrite.servlet.config.HttpConfigurationProvider;

import javax.servlet.ServletContext;

public class PrettyfacesConfig extends HttpConfigurationProvider {
    @Override
    public Configuration getConfiguration(ServletContext servletContext) {
        return ConfigurationBuilder.begin();
    }

    @Override
    public int priority() {
        return 10;
    }
}

package com.eightbitcenter.dao;

import com.eightbitcenter.model.ModelMarker;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;

public interface GenericDao<T> {
    void save(T t);
    void delete(T t);
}
package com.eightbitcenter.dao;

import com.eightbitcenter.config.SpringMongoConfig;
import com.eightbitcenter.model.Post;
import org.apache.log4j.Logger;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class PostDAO extends GenericDaoImpl<Post> {
    public static Logger LOG = Logger.getLogger(PostDAO.class);
    public static int counter = 0;
    ApplicationContext ctx =
            new AnnotationConfigApplicationContext(SpringMongoConfig.class);
    MongoOperations mongoOperation = (MongoOperations) ctx.getBean("mongoTemplate");

    public void savePost(Post post){
        LOG.info("savePost()") ;
         mongoOperation.save(post);
    }

    public List<Post> getPosts(String userId){
        LOG.info("getPosts() - STEP 1") ;
        Query query = new Query(Criteria.where("userId").is(userId));
        List<Post> resultList = mongoOperation.find(query, Post.class);
        return resultList;
    }
    
    public void deletePost(Post post){
        LOG.info("deletesavePost()") ;
         mongoOperation.remove(post);
    }
    
    public void updatePost(Post post,String newTopic,String newMessage,String newColor)
    {
    	LOG.info("updatePost()") ;
    	if(newTopic!=null)post.setTopic(newTopic);
    	if(newMessage!=null)post.setMessage(newMessage);
    	if(newColor!=null)post.setColor(newColor);
    	mongoOperation.save(post);
 
    }

}

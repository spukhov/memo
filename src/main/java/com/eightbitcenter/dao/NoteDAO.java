package com.eightbitcenter.dao;

import com.eightbitcenter.config.SpringMongoConfig;
import com.eightbitcenter.model.Note;
import org.apache.log4j.Logger;
import org.joda.time.DateTime;
import org.joda.time.DateTimeFieldType;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
public class NoteDAO extends GenericDaoImpl<Note> {
    public static Logger LOG = Logger.getLogger(NoteDAO.class);
    public static int counter = 0;

    public void del(Note note){
        delete(note);
    }
    public void persist(Note note){
        save(note);
    }

    public List<Note> find(String userId, Date date){
        LOG.info("find(): "
        +"\n    date - " + date) ;
        Date minDate = new DateTime(date).withTimeAtStartOfDay().toDate();
        Date maxDate = new DateTime(date).plusDays(1).withTimeAtStartOfDay().toDate();
        Query query = new Query(Criteria.where("userId").is(userId).and("date").gte(minDate).lt(maxDate));
        List<Note> resultList = mongoOperation.find(query, Note.class);
        return resultList;
    }

    /** Will update table only after adding a post */
    public Map<DateTime, Integer> findNotesForMonth(String userId, Date date){
        counter++;
        LOG.info("findNotesForMonth()" + counter) ;
        Map<DateTime, Integer> resultMap = new HashMap<DateTime, Integer>();
        Date minDate = new DateTime(date).withDayOfMonth(1).withTimeAtStartOfDay().toDate();
        Date maxDate = new DateTime(date).withMonthOfYear(new DateTime(date).get(DateTimeFieldType.monthOfYear()) + 1)
                .withDayOfMonth(1)
                .withTimeAtStartOfDay().toDate();
        Query query = new Query(Criteria.where("userId").is(userId).and("date").gte(minDate).lt(maxDate));
        List<Note> resultList = mongoOperation.find(query, Note.class);
        for (Note note : resultList) {
            DateTime noteDate = new DateTime(note.getDate()).withTimeAtStartOfDay();
            Integer noteNumber = resultMap.get(noteDate);
            if (noteNumber!=null && noteNumber>0){
                resultMap.put(noteDate, ++noteNumber);
            } else resultMap.put(noteDate, 1);
        }
        return resultMap;
    }
    
    public void update(Note note){
    	LOG.info("update()") ;
    	mongoOperation.save(note);
    }

    public boolean isDayEmpty(String userId, Date date){
        Date minDate = new DateTime(date).withTimeAtStartOfDay().toDate();
        Date maxDate = new DateTime(date).plusDays(1).withTimeAtStartOfDay().toDate();
        Query query = new Query(Criteria.where("userId").is(userId).and("date").gte(minDate).lt(maxDate));
        return mongoOperation.findOne(query, Note.class)==null;
    }

}

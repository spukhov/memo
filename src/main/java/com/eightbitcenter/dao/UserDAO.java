package com.eightbitcenter.dao;

import com.eightbitcenter.config.SpringMongoConfig;
import com.eightbitcenter.model.User;
import org.apache.log4j.Logger;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

@Repository
public class UserDAO {
    private String currentUserId;

    public static Logger LOG = Logger.getLogger(UserDAO.class);

    ApplicationContext ctx =
            new AnnotationConfigApplicationContext(SpringMongoConfig.class);
    MongoOperations mongoOperation = (MongoOperations) ctx.getBean("mongoTemplate");

    public void register(User user){
        LOG.info("register()");
         mongoOperation.save(user);
    }
    

    public boolean validateLogin(User user){
        /*LOG.info("validateLogin() - email: " + user.getEmail() + "; password: " + user.getPassword());
        Query searchUserQuery = new Query(Criteria.where("email").is(user.getEmail()).and("password").is(user.getPassword()));
        User currentUser = mongoOperation.findOne(searchUserQuery, User.class);
        if (currentUser!=null) {
            currentUserId = currentUser.getId();
            LOG.info("currentUserId: "+currentUserId);
            return true;
        }
        else return false;*/
        return true;
    }

    
    public User findUserByName(String email)
    {
    	Query query = new Query(Criteria.where("email").is(email));
        User user = (User) mongoOperation.findOne(query, User.class);
		return user;
    	
    }
    public String getId(){
        LOG.info("getId() - Id saved after logging in. (lazy load)");
        return currentUserId;
    }
}

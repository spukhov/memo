package com.eightbitcenter.dao;


import com.eightbitcenter.model.Moneybox;
import com.eightbitcenter.model.Post;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class MoneyboxDAO extends GenericDaoImpl<Moneybox>{
    public void persist(Moneybox m){
        save(m);
    }
    public void del(Moneybox m){
        delete(m);
    }
    public List<Moneybox> find(String userId){
        Query query = new Query(Criteria.where("userId").is(userId));
        List<Moneybox> resultList = mongoOperation.find(query, Moneybox.class);
        System.out.println(userId + "========" + resultList);
        return resultList;
    }

    public void updateActualAmount(double n) {
        Query query = new Query();
        Update update = new Update();
        update.set("actualAmount", n);
        mongoOperation.updateFirst(query, update, Moneybox.class);
    }
}

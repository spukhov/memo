package com.eightbitcenter.dao;


import com.eightbitcenter.config.SpringMongoConfig;
import com.eightbitcenter.model.ModelMarker;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Repository;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;

public class GenericDaoImpl<T> implements GenericDao<T> {
    ApplicationContext ctx =
            new AnnotationConfigApplicationContext(SpringMongoConfig.class);
    MongoOperations mongoOperation = (MongoOperations) ctx.getBean("mongoTemplate");
    private Class< T > type;

    public GenericDaoImpl() {
        Type t = getClass().getGenericSuperclass();
        ParameterizedType pt = (ParameterizedType) t;
        type = (Class) pt.getActualTypeArguments()[0];
    }
    public void save(T t) {
        System.out.println("Generic save " + t.getClass());
        mongoOperation.save(t);
    }

    public void delete(final T t) {
        mongoOperation.remove(t);
    }

}
